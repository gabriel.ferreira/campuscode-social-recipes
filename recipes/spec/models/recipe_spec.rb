require 'rails_helper'

describe Recipe do
  context 'list most favorites' do
    it 'but not the recipes without likes' do
      recipes = create_list(:recipe, 21)
      recipes.each.with_index do |recipe, index|
        recipe.tasters_count = index+1
        recipe.save
      end

      not_favorite_recipe = recipes[0]
      recipes = Recipe.most_favorites
      expect(recipes.include? not_favorite_recipe).to be_falsy
    end
  end
end
