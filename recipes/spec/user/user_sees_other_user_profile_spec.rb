require 'rails_helper'

feature 'user sees other user profile' do
  scenario 'successfully' do
    user = create(:user)
    another_user = create(:user)

    another_user_recipe = create(:recipe, user:another_user)
    login_as_user(user)

    visit user_path(another_user)

    expect(page).to have_content("Perfil de #{another_user.name}")
    expect(page).to have_content another_user.email
    expect(page).to have_content another_user.location
    expect(page).to have_content another_user_recipe.name
    expect(page).to_not have_content 'Editar'
    expect(page).to_not have_content 'Deletar'
    expect(page).to_not have_content 'Meus dados'
  end
end
