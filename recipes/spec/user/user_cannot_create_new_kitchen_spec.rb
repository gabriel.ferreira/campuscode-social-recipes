require 'rails_helper'

feature 'user cannot create new kitchen' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    visit new_kitchen_path

    expect(current_path).to_not eq(new_kitchen_path)
    # This line will not work, because devise automatically redirects
    # logged users to their home page, or root_path
    # expect(current_path).to eq( new_user_session_path )
  end
end
