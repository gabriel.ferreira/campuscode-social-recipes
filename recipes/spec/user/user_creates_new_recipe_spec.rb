require 'rails_helper'

feature 'user creates a new recipe' do
  scenario 'successfully' do
    user = create(:user)
    preference = create(:food_preference)
    recipe = build(:recipe, food_preference: preference)

    create_new_recipe_by_browser(recipe, login: user)
    expect(recipe).to be_valid
    click_on 'Criar receita'
    check_recipe_is_present(recipe)
  end

  scenario 'with long name' do
    user = create(:user)
    recipe = build(:recipe_with_long_title)
    create_new_recipe_by_browser(recipe, login: user)
    click_on 'Criar receita'
    expect(page).to have_content('The name is too long.')
  end

  scenario 'with blank fields' do
    user = create(:user)
    login_as_user(user)
    visit new_recipe_path
    click_on 'Criar receita'
    expect(page).to have_content('Must give a recipe name.')
    expect(page).to have_content("Must fill in the recipe\'s ingredients")
    expect(page).to have_content("Must fill in the recipe\'s preparation")
  end

  scenario 'with photo' do
    user = create(:user)
    food_preference = create(:food_preference)
    recipe = build(:recipe, food_preference: food_preference)
    create_new_recipe_by_browser(recipe, login: user)
    attach_file 'recipe[photo]', Rails.root + 'spec/photos/massas.png'
    click_on 'Criar receita'
    recipe = Recipe.last

    expect(recipe.photo.url).to_not be_nil
  end

  scenario 'and can see it at profile' do
    user = create(:user)
    recipe = build(:recipe)

    create_new_recipe_by_browser(recipe, login: user)

    click_on 'Criar receita'

    visit user_path(user)
    expect(current_path).to eq(user_path(user))
    click_on recipe.name
    check_recipe_is_present(recipe)
  end
end
