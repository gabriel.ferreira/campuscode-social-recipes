require 'rails_helper'

feature 'user edits his recipes' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    recipe = create(:recipe, user:user)
    new_recipe_name = "Novo nome"
    click_on 'Minhas receitas'
    within '.last-recipes' do
      click_on recipe.name
    end

    click_on 'Editar'
    expect(current_path).to eq(edit_recipe_path(recipe))
    fill_in 'recipe[name]', with: new_recipe_name

    click_on 'Salvar alterações'

    expect(page).to have_content(new_recipe_name)
  end

  scenario "but not other\'s recipes" do
    user = create(:user)

    another_user = create(:user)
    anyones_recipe = create(:recipe, user: another_user)

    login_as_user(user)
    expect(current_path).to eq(user_path(user))

    visit root_path
    expect(page).to have_content anyones_recipe.name
    expect(page).to_not have_content 'Editar'

    within '.last-recipes' do
      click_on anyones_recipe.name
    end

    expect(page).to_not have_content 'Editar'
  end

  scenario 'when viewing his recipes' do
    user = create(:user)
    login_as_user(user)
    recipe = create(:recipe, user: user)

    visit recipe_path(recipe)

    click_on 'Editar'

    expect(current_path).to eq(edit_recipe_path(recipe))
  end
end
