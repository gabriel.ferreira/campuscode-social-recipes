require 'rails_helper'

feature 'user sees home page' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)
    visit user_path(user)
    expect(page).to have_content('Social Recipes')
    expect(page).to have_content('Início')
    expect(page).to have_content('Cadastrar receita')
    expect(page).to have_content('Minhas receitas')
    expect(page).to have_content('Sair')

    expect(page).to have_content('Últimas receitas')
    expect(page).to have_content('Favoritas dos usuários')
  end

  scenario 'and click on recipe' do
    user = create(:user)
    login_as_user(user)
    recipe = create(:recipe_with_photo, user: user)

    visit user_path(user)
    click_on recipe.name

    expect(current_path).to eq(recipe_path(recipe))
    expect(page).to have_content("Enviado por #{user.name}")
    within('.recipe-title') do
      expect(page).to have_content(recipe.name)
    end
    within('.recipe-data') do
      expect(page).to have_content(recipe.kitchen.name)
      expect(page).to have_content(recipe.food_type.name)
      expect(page).to have_content(recipe.food_preference.name)
      expect(page).to have_content(recipe.total_people)
      expect(page).to have_content(recipe.total_time)
      expect(page).to have_content(recipe.difficulty)
    end
    within('.recipe-ingredients') do
      expect(page).to have_content(recipe.ingredients)
    end
    within('.recipe-body') do
      expect(page).to have_content(recipe.preparation)
    end
    within('.recipe-image') do
      expect(page.find('#recipe-photo')['src']).to have_content recipe.photo
    end
  end

  scenario 'sees a recipe and returns to user' do
    user = create(:user)
    login_as_user(user)
    recipe = create(:recipe, user: user)

    visit user_path(user)
    click_on recipe.name

    click_on user.name

    expect(current_path).to eq(user_path(user))
  end
end
