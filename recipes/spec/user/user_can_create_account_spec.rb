require 'rails_helper'

feature 'user create account' do
  scenario 'successfully' do
    user = build(:user)

    visit new_user_registration_path

    fill_in 'Name', with: user.name
    fill_in 'Location', with: user.location
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    fill_in 'Password confirmation', with: user.password
    within 'form' do
      click_on 'Sign up'
    end

    expect(page).to have_content("Perfil de #{user.name}")
    expect(page).to have_content user.email
    expect(page).to have_content user.location
  end

  scenario 'with repeated name in database' do
    user = create(:user)

    visit new_user_registration_path

    fill_in 'Name', with: user.name
    fill_in 'Location', with: user.location
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    fill_in 'Password confirmation', with: user.password
    within 'form' do
      click_on 'Sign up'
    end

    expect(page).to have_content("Ops! This name has already been taken.")
  end

  scenario 'with blank name or location' do
    user = build(:blank_user)

    visit new_user_registration_path

    fill_in 'Name', with: user.name
    fill_in 'Location', with: user.location
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    fill_in 'Password confirmation', with: user.password
    within 'form' do
      click_on 'Sign up'
    end

    expect(page).to have_content("Please, present yourself!")
    expect(page).to have_content("Please, tell us at least your city!")
  end
end
