require 'rails_helper'

feature 'user can login' do
  scenario 'successfully' do
    user = create(:user)

    visit root_path

    expect(page).to_not have_content('Logout')
    expect(page).to have_content('Sign up')
    click_on('Login')

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Log in'

    expect(current_path).to eq(user_path(user))
  end

  scenario 'and logout' do
    user = create(:user)
    login_as_user(user)
    visit root_path

    expect(page).to_not have_content('Login')
    expect(page).to_not have_content('Sign up')

    click_on('Logout')

    expect(current_path).to eq(root_path)

    expect(page).to have_content('Login')
    expect(page).to have_content('Sign up')
  end
end
