require 'rails_helper'

feature 'user cannot create new kitchen' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    visit new_food_type_path

    expect(current_path).to_not eq(new_food_type_path)
  end
end
