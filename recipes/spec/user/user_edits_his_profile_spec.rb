require 'rails_helper'

feature 'user edits his profile' do
  scenario 'successfully' do
    user = create(:user)
    new_name = 'Novo nome'
    visit user_path(user)
    expect(page).to_not have_content 'Meus dados'

    login_as_user(user)

    visit user_path(user)

    click_on 'Meus dados'

    expect(current_path).to eq(edit_user_registration_path(user))
    fill_in 'Name', with: new_name
    fill_in 'Current password', with: user.password
    click_on 'Update'

    expect(page).to have_content("Perfil de #{new_name}")
  end
end
