require 'rails_helper'

feature 'user cannot create new preference' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    visit new_food_preference_path

    expect(current_path).to_not eq(new_food_preference_path)
    # This line will not work, because devise automatically redirects
    # logged users to their home page, or root_path
    # expect(current_path).to eq( new_user_session_path )
  end
end
