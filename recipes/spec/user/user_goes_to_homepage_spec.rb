require 'rails_helper'

feature 'user goes to home page' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    click_on 'Minhas receitas'

    expect(page).to have_content('Social Recipes')
    expect(page).to have_content('Início')
    expect(page).to have_content('Cadastrar receita')
    expect(page).to have_content('Minhas receitas')
    expect(page).to have_content('Sair')

    expect(current_path).to eq(user_path(user))

    expect(page).to have_content('Últimas receitas')
    expect(page).to have_content('Favoritas dos usuários')
  end

  scenario 'and sees his recipes' do
    user = create(:user)
    recipes = create_list(:recipe, 3, user: user)
    login_as_user(user)

    click_on 'Minhas receitas'

    expect(current_path).to eq(user_path(user))
    recipes.each do |recipe|
      check_recipe_is_present(recipe)
    end
  end

  scenario "but can\'t see other user\'s recipes" do
    user = create(:user)
    other_user = create(:user)
    other_user_recipe = create(:random_recipe, user: other_user)
    login_as_user(user)

    expect(current_path).to eq(user_path(user))
    expect(page).to_not have_content(other_user_recipe)
  end
end
