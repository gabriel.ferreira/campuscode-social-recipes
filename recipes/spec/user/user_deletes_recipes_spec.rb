require 'rails_helper'

feature 'user deletes his recipes' do
  scenario 'successfully' do
    user = create(:user)
    login_as_user(user)

    recipe = create(:recipe, user:user)

    click_on 'Minhas receitas'
    within '.last-recipes' do
      click_on recipe.name
    end

    click_on 'Deletar'

    expect(current_path).to eq(user_path(user))
    expect(page).to_not have_content(recipe.name)
  end

  scenario "but not other\'s recipes" do
    user = create(:user)

    another_user = create(:user)
    anyones_recipe = create(:recipe, user: another_user)

    login_as_user(user)
    expect(current_path).to eq(user_path(user))

    visit root_path
    expect(page).to have_content anyones_recipe.name
    expect(page).to_not have_content 'Deletar'

    within('.last-recipes') do
      click_on anyones_recipe.name
    end

    expect(page).to_not have_content 'Deletar'

  end

  scenario 'when viewing his recipes' do
    user = create(:user)
    login_as_user(user)
    recipe = create(:recipe, user: user)

    visit recipe_path(recipe)

    click_on 'Deletar'

    expect(current_path).to eq(user_path(user))
  end
end
