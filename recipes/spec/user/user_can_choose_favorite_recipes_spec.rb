require 'rails_helper'

feature 'user can choose favorite recipes' do
  scenario 'successfully' do
    user = create(:user)
    recipe = create(:recipe, user: user)
    login_as_user(user)

    visit root_path

    within '.last-recipes > .recipe' do
      expect(page).to have_content(recipe.name)
      click_on 'Marcar favorita'
    end

    visit user_path(user)

    within '.favorites > .recipe' do
      expect(page).to have_content(recipe.name)
    end
  end

  scenario 'between other recipes' do
    user = create(:user)
    recipe = create_list(:random_recipe, 2, user: user)
    selected_recipe = create(:random_recipe, user: user)
    other_recipes = create_list(:random_recipe, 2)

    login_as_user(user)

    visit root_path

    within '.last-recipes' do
      selected_as_favorite = page.find('.recipe', text: selected_recipe.name)
      selected_as_favorite.click_on 'Marcar favorita'
    end

    visit user_path(user)

    within '.favorites' do
      expect(page).to have_content(selected_recipe.name)
      (recipe + other_recipes).each do |single_recipe|
        expect(page).to_not have_content(single_recipe.name)
      end
    end
  end

  scenario 'created by other users' do
    user = create(:user)
    anyone = create(:user)
    login_as_user(user)
    anyones_recipe = create(:recipe, user: anyone)
    visit root_path

    within '.last-recipes > .recipe' do
      expect(page).to have_content(anyones_recipe.name)
      click_on 'Marcar favorita'
    end

    visit user_path(user)

    within '.favorites' do
      expect(page).to have_content(anyones_recipe.name)
    end
  end

  scenario 'and unselect it' do
    user = create(:user)
    recipe = create(:recipe, user: user)
    login_as_user(user)

    visit root_path

    within '.last-recipes > .recipe' do
      expect(page).to have_content(recipe.name)
      click_on 'Marcar favorita'
    end

    visit user_path(user)

    within '.favorites > .recipe' do
      expect(page).to have_content(recipe.name)
      click_on 'Desmarcar'
    end

    within '.favorites' do
      expect(page).to_not have_content(recipe.name)
    end
  end
end
