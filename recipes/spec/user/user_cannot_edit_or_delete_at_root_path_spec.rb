require 'rails_helper'

feature 'user cannot edit or delete at root path' do
  scenario 'directly' do
    user = create(:user)
    login_as_user(user)

    recipe = create(:recipe, user: user)
    visit root_path

    expect(page).to_not have_content('Editar')
    expect(page).to_not have_content('Deletar')
  end
end
