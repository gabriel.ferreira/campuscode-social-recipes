require 'rails_helper'

feature 'visitor cannot edit recipe' do
  scenario 'directly' do
    recipe = create(:recipe)
    visit edit_recipe_path(recipe)

    expect(page).to_not have_content('Salvar alterações')
    expect(current_path).to eq(new_user_session_path)
  end

  scenario 'by root path edit button' do
    recipe = create(:recipe)
    visit root_path

    expect(page).to_not have_content('Editar')
  end

  scenario 'by viewing an recipe' do
    recipe = create(:recipe)
    visit root_path

    within '.last-recipes' do
      click_on recipe.name
    end

    expect(current_path).to eq(recipe_path(recipe))
    expect(page).to_not have_content('Editar')
  end
end
