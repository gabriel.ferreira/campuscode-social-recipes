require 'rails_helper'

feature 'visitor cannot delete recipe' do
  scenario 'by root path delete button' do
    recipe = create(:recipe)
    visit root_path

    expect(page).to_not have_content('Deletar')
  end

  scenario 'by viewing an recipe' do
    recipe = create(:recipe)
    visit root_path

    within '.last-recipes' do
      click_on recipe.name
    end

    expect(current_path).to eq(recipe_path(recipe))
    expect(page).to_not have_content('Deletae')
  end
end
