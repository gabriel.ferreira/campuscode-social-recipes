require 'rails_helper'

feature 'visitor chooses recipe by preference' do
  scenario 'successfully' do
    user = create(:user)

    without_lactose = create(:food_preference, name: 'Sem lactose')
    vegetarian      = create(:food_preference, name: 'Vegetarianos')

    without_lactose_recipe = create(:recipe, name: 'Receita sem lactose',
                                             food_preference: without_lactose,
                                             user:user)
    vegetarian_recipe = create(:recipe, name: 'Receita vegetariana',
                                        food_preference: vegetarian,
                                        user:user )

    visit root_path

    expect(page).to have_content("Preferência")
    expect(page).to have_content('Sem lactose')
    expect(page).to have_content('Vegetarianos')

    click_on 'Sem lactose'

    expect(page).to have_content('Sem lactose')
    expect(page).to have_content(without_lactose_recipe.name)
    expect(page).to_not have_content(vegetarian_recipe.name)
  end

  scenario 'and switch through preferences' do
    user = create(:user)
    without_lactose = create(:food_preference, name: 'Sem lactose')
    vegetarian      = create(:food_preference, name: 'Vegetarianos')

    without_lactose_recipe = create(:recipe, name: 'Receita sem lactose',
                                             food_preference: without_lactose,
                                             user:user)
    vegetarian_recipe = create(:recipe, name: 'Receita vegetariana',
                                        food_preference: vegetarian,
                                        user:user )

    visit root_path

    expect(page).to have_content("Preferência")
    expect(page).to have_content('Sem lactose')
    expect(page).to have_content('Vegetarianos')

    click_on 'Sem lactose'

    expect(page).to have_content("Preferência")
    expect(page).to have_content('Sem lactose')
    expect(page).to have_content('Vegetarianos')

    click_on 'Vegetarianos'

    expect(page).to have_content('Vegetarianos')
    expect(page).to_not have_content(without_lactose_recipe.name)
    expect(page).to have_content(vegetarian_recipe.name)
  end

  scenario 'and go back to home' do
    user = create(:user)
    without_lactose = create(:food_preference, name: 'Sem lactose')
    vegetarian      = create(:food_preference, name: 'Vegetarianos')

    without_lactose_recipe = create(:recipe, food_preference: without_lactose,
                                             user:user)
    vegetarian_recipe = create(:recipe, food_preference: vegetarian,
                                        user:user )
    visit root_path

    expect(page).to have_content("Preferência")
    expect(page).to have_content('Sem lactose')
    expect(page).to have_content('Vegetarianos')

    click_on 'Sem lactose'
    click_on 'Início'

    expect(current_path).to eq(root_path)
  end
end
