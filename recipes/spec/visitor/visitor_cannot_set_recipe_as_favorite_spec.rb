require 'rails_helper'

feature 'visitor cannot set recipe as favorite' do
  scenario 'successfully' do
    recipe = create(:recipe)
    visit root_path

    within '.last-recipes > .recipe' do
      expect(page).to have_content(recipe.name)
      expect(page).to_not have_content('Marcar favorita')
    end
  end

  scenario 'when seeing other users profile' do
    recipe = create(:recipe)
    visit user_path(recipe.user)

    within '.last-recipes > .recipe' do
      expect(page).to have_content(recipe.name)
      expect(page).to_not have_content('Marcar favorita')
    end
  end
end
