require 'rails_helper'

feature 'visitor sees root path' do
  scenario 'successfully' do
    visit root_path
    expect(page).to have_content('Social Recipes')
  end
end
