require 'rails_helper'

feature 'visitor sees the most favorite recipes' do
  scenario 'successfully' do
    user = create(:user)
    another_user = create(:user)
    third_user = create(:user)
    forth_user = create(:user)

    recipes = create_list(:random_recipe, 5, user: user)

    user.favorites         << recipes[3]
    another_user.favorites << [recipes[3], recipes[2]]
    third_user.favorites   << [recipes[3], recipes[2], recipes[1]]
    another_user.favorites << [recipes[3], recipes[2], recipes[1], recipes[0]]

    visit root_path

    within '.favorites' do
      expect(page).to have_content(recipes[0].name)
      expect(page).to have_content(recipes[1].name)
      expect(page).to have_content(recipes[2].name)
      expect(page).to have_content(recipes[3].name)
      expect(page).to_not have_content(recipes[4].name)
    end
  end
end
