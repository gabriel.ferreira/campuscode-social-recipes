require 'rails_helper'

feature 'visitor cannot create new kitchen' do
  scenario 'successfully' do
    visit new_kitchen_path
    expect(current_path).to eq(new_user_session_path)
  end
end
