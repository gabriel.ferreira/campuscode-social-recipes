require 'rails_helper'

feature 'visitor chooses recipe by preference' do
  scenario 'successfully' do
    user = create(:user)

    italian  = create(:kitchen, name: 'Italiana')
    chinese  = create(:kitchen, name: 'Chinesa')

    italian_recipe = create(:recipe, name: 'Receita chinesa',
                                             kitchen: italian,
                                             user:user)
    chinese_recipe = create(:recipe, name: 'Receita italiana',
                                        kitchen: chinese,
                                        user:user )

    visit root_path

    expect(page).to have_content("Cozinha")
    expect(page).to have_content('Italiana')
    expect(page).to have_content('Chinesa')

    click_on 'Italiana'

    expect(page).to have_content('Italiana')
    expect(page).to have_content(italian_recipe.name)
    expect(page).to_not have_content(chinese_recipe.name)
  end

  scenario 'and switch through preferences' do
    user = create(:user)

    italian  = create(:kitchen, name: 'Italiana')
    chinese  = create(:kitchen, name: 'Chinesa')

    italian_recipe = create(:recipe, name: 'Receita chinesa',
                                             kitchen: italian,
                                             user:user)
    chinese_recipe = create(:recipe, name: 'Receita italiana',
                                        kitchen: chinese,
                                        user:user )
    visit root_path

    expect(page).to have_content("Cozinha")
    expect(page).to have_content('Italiana')
    expect(page).to have_content('Chinesa')

    click_on 'Italiana'

    expect(page).to have_content('Italiana')
    expect(page).to have_content(italian_recipe.name)
    expect(page).to_not have_content(chinese_recipe.name)

    click_on 'Chinesa'

    expect(page).to have_content('Chinesa')
    expect(page).to_not have_content(italian_recipe.name)
    expect(page).to have_content(chinese_recipe.name)
  end

  scenario 'and go back to home' do
    user = create(:user)

    italian  = create(:kitchen, name: 'Italiana')
    chinese  = create(:kitchen, name: 'Chinesa')

    italian_recipe = create(:recipe, name: 'Receita chinesa',
                                             kitchen: italian,
                                             user:user)
    chinese_recipe = create(:recipe, name: 'Receita italiana',
                                        kitchen: chinese,
                                        user:user )
    visit root_path

    expect(page).to have_content("Cozinha")
    expect(page).to have_content('Italiana')
    expect(page).to have_content('Chinesa')

    click_on 'Italiana'
    click_on 'Início'

    expect(current_path).to eq(root_path)
  end
end
