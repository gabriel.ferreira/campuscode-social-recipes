require 'rails_helper'

feature 'visitor cannot create a new recipe' do
  scenario 'successfully' do
    recipe = build(:recipe)

    visit new_recipe_path

    expect(current_path).to eq(new_user_session_path)
  end

  scenario 'by menu bar link' do
    already_existent_recipe = create(:recipe)
    visit root_path

    expect(page).to_not have_content('Criar receita')

    within '.last-recipes' do
      click_on already_existent_recipe.name
    end
    expect(page).to_not have_content('Criar receita')
  end
end
