require 'rails_helper'

feature 'visitor sees at home page' do
  scenario 'last twenty recipes successfully' do
    recipes = create_list(:random_recipe, 20)

    visit root_path

    expect(page).to have_content('Últimas receitas')
    expect(page).to have_content('Favoritas dos usuários')

    recipes.each do |recipe|
      check_recipe_is_present(recipe)
    end

  end

  scenario 'only the last twenty recipes' do
    old_recipe = create(:random_recipe)
    recipes = create_list(:random_recipe, 20)

    visit root_path

    expect(page).to have_content('Últimas receitas')
    expect(page).to have_content('Favoritas dos usuários')

    within '.last-recipes' do
      expect(page).to_not have_content( old_recipe.name )
    end
  end

  scenario 'last twenty recipes after reload' do
    first_recipe = create(:random_recipe)
    recipes = create_list(:random_recipe, 19)

    visit root_path

    (recipes + [first_recipe]).each do |recipe|
      check_recipe_is_present(recipe)
    end

    new_recipe = create(:random_recipe)
    visit root_path

    within '.last-recipes' do
      expect(page).to_not have_content( first_recipe.name )
    end
    
    (recipes + [new_recipe]).each do |recipe|
      check_recipe_is_present(recipe)
    end


  end
end
