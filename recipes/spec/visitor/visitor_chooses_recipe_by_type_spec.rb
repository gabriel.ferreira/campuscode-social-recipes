require 'rails_helper'

feature 'visitor chooses recipe by food type' do
  scenario 'successfully' do
    user = create(:user)

    meat = create(:food_type, name: 'Carnes')
    soup = create(:food_type, name: 'Sopas')

    meat_recipe = create(:recipe, name: 'Receita sem lactose',
                                  food_type: meat,
                                  user:user)
    soup_recipe = create(:recipe, name: 'Receita vegetariana',
                                        food_type: soup,
                                        user:user )

    visit root_path

    expect(page).to have_content("Tipo")
    expect(page).to have_content('Carnes')
    expect(page).to have_content('Sopas')

    click_on 'Sopas'

    expect(page).to have_content('Sopas')
    expect(page).to have_content(soup_recipe.name)
    expect(page).to_not have_content(meat_recipe.name)
  end

  scenario 'and switch through preferences' do
    user = create(:user)

    meat = create(:food_type, name: 'Carnes')
    soup = create(:food_type, name: 'Sopas')

    meat_recipe = create(:recipe, name: 'Receita sem lactose',
                                  food_type: meat,
                                  user:user)
    soup_recipe = create(:recipe, name: 'Receita vegetariana',
                                        food_type: soup,
                                        user:user )

    visit root_path

    expect(page).to have_content("Tipo")
    expect(page).to have_content('Carnes')
    expect(page).to have_content('Sopas')

    click_on 'Sopas'

    expect(page).to have_content('Sopas')
    expect(page).to have_content(soup_recipe.name)
    expect(page).to_not have_content(meat_recipe.name)

    click_on 'Carnes'

    expect(page).to have_content('Carnes')
    expect(page).to_not have_content(soup_recipe.name)
    expect(page).to have_content(meat_recipe.name)
  end

  scenario 'and go back to home' do
    user = create(:user)

    meat = create(:food_type, name: 'Carnes')
    soup = create(:food_type, name: 'Sopas')

    meat_recipe = create(:recipe, name: 'Receita sem lactose',
                                  food_type: meat,
                                  user:user)
    soup_recipe = create(:recipe, name: 'Receita vegetariana',
                                        food_type: soup,
                                        user:user )
    visit root_path
    expect(page).to have_content("Tipo")
    expect(page).to have_content('Carnes')
    expect(page).to have_content('Sopas')

    click_on 'Sopas'
    click_on 'Início'

    expect(current_path).to eq(root_path)
  end
end
