require 'rails_helper'

feature 'visitor cannot create new food type' do
  scenario 'successfully' do
    visit new_food_type_path
    expect(current_path).to eq(new_user_session_path)
  end
end
