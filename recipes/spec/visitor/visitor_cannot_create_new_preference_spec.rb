require 'rails_helper'

feature 'visitor cannot create new preference' do
  scenario 'successfully' do
    visit new_food_preference_path
    expect(current_path).to eq(new_user_session_path)
  end
end
