require 'rails_helper'

RSpec.describe RecipeMailer do
  include Rails.application.routes.url_helpers
  before (:each) do
    @user = create(:user)
    @recipe = create(:recipe)
  end

  describe 'send_recipe_to_friend' do
    let(:mail) { RecipeMailer.send_recipe_to_friend(@recipe, @user.email) }

    it 'renders the subject' do
      expect(mail.subject).to eq('Uma receita para você!')
    end

    it 'renders the sender' do
      expect(mail.from).to eq(['socialrecipes.gabrielferreira@gmail.com'])
    end

    it 'renders the receiver' do
      expect(mail.to).to eq([@user.email])
    end

    it 'renders the body as UTF-8' do
      expect(mail.body.encoded.is_utf8?).to be_truthy
    end
  end
end
