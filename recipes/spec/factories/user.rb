FactoryGirl.define do
  factory :user do
    sequence(:name) {|n| "User #{n}"}
    sequence(:email) {|n| "emailsample#{n}@mail.com"}
    location 'São Paulo/SP'
    password "14563241456"

    factory :admin do
      admin true
    end

    factory :blank_user do
      name ''
      location ''
    end
  end
end
