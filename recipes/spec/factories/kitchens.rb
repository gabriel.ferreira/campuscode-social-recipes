FactoryGirl.define do
  factory :kitchen do
    sequence(:name) {|n| "Cozinha #{n}"}

    factory :kitchen_with_long_name do
      name 'Too long'*4
    end
  end
end
