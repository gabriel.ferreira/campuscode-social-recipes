FactoryGirl.define do
  factory :food_type do
    sequence(:name) { |n| "Tipo de comida #{n}" }

    factory :food_type_with_long_name do
      name 'Too long'*4
    end
  end
end
