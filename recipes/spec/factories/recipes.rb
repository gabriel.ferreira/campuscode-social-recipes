FactoryGirl.define do
  factory :recipe do

    trait :ingredients do
      ingredients "1 ovo 200g de fermento biologico açucar 2kg de farinha de trigo 8 colheres de oleo 1 colher de sal"
    end

    trait :preparation do
      preparation "Colocar o fermento em uma tijela e misturar tudo ate virar liquido... bla... bla... bla... mandar pro forno... esperar... bla... PRONTO!!!"
    end

    name            'Receita de massa para pizza'
    total_people    4
    total_time      35.minutes
    difficulty      :medium
    created_at      { Time.now }
    kitchen
    ingredients
    preparation
    food_preference
    food_type
    user




    factory :recipe_with_long_title do
      name 'b'*40
    end

    factory :recipe_with_invalid_difficulty do
      difficulty   'other_thing'
    end

    factory :recipe_with_photo do
      photo {fixture_file_upload(Rails.root.join('spec', 'photos', 'massas.png'), 'image/png')}
    end

    factory :random_recipe do
      sequence(:name)  { |n| "Receita #{n}" }
    end
  end
end
