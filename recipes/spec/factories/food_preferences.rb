FactoryGirl.define do
  factory :food_preference do
    sequence(:name) {|n| "Preferência #{n}"}

    factory :food_preference_with_long_name do
      name 'Too long'*4
    end
  end
end
