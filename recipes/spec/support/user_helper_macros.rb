module UserHelperMacros
  def login_as_user(user)
    return false if user.nil? or !user.persisted?
    visit new_user_session_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Log in'
  end

  def login_as_admin(admin)
    admin ||= create(:admin)
    visit new_user_session_path
    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password
    click_on 'Log in'
  end

end
