module RecipeHelperMacros
  def create_new_recipe_by_browser(recipe, hash = {})
    if hash[:login]
      hash[:login] ||= create(:user)
      login_as_user(hash[:login])
    end
    visit new_recipe_path

    fill_in 'recipe[name]', with: recipe.name
    select  recipe.kitchen.name, from: 'recipe[kitchen_id]'
    select  recipe.food_preference.name, from: 'recipe[food_preference_id]'
    select  recipe.food_type.name, from: 'recipe[food_type_id]'
    fill_in 'recipe[total_people]', with: recipe.total_people
    fill_in 'recipe[total_time]', with: recipe.total_time
    select  recipe.difficulty, from: 'recipe[difficulty]'
    fill_in 'recipe[ingredients]', with: recipe.ingredients
    fill_in 'recipe[preparation]', with: recipe.preparation

  end

  def check_recipe_is_present(recipe)

    expect(page).to have_content(recipe.name)
    unless is_at_root_or_profile?( recipe.user )
      expect(page).to have_content(recipe.kitchen.name)
      expect(page).to have_content(recipe.food_type.name)
      expect(page).to have_content(recipe.food_preference.name)
      expect(page).to have_content(recipe.total_people)
      expect(page).to have_content(recipe.total_time)
      expect(page).to have_content(recipe.difficulty)
      expect(page).to have_content(recipe.ingredients)
      expect(page).to have_content(recipe.preparation)
    end

  end

  def check_recipe_is_not_present(recipe)

    expect(page).to_not have_content(recipe.name)
    unless is_at_root_or_profile?( recipe.user )
      expect(page).to_not have_content(recipe.kitchen.name)
      expect(page).to_not have_content(recipe.food_type.name)
      expect(page).to_not have_content(recipe.food_preference.name)
      expect(page).to_not have_content(recipe.total_people)
      expect(page).to_not have_content(recipe.total_time)
      expect(page).to_not have_content(recipe.difficulty)
      expect(page).to_not have_content(recipe.ingredients)
      expect(page).to_not have_content(recipe.preparation)
    end

  end

  def is_at_root_or_profile?(user)
    current_path == root_path or current_path == user_path(user)
  end
end
