require 'rails_helper'

feature 'admin creates a new food preference' do
  scenario 'successfully' do
    admin = create(:admin)
    login_as_admin(admin)

    food_preference = build(:food_preference)
    visit new_food_preference_path

    fill_in 'food_preference[name]', with: food_preference.name
    click_on 'Criar nova preferência'

    expect(page).to have_content(food_preference.name)

    # checks the aside menu, where are all registered food_preferences
    within('aside') do
      expect(page).to have_content(food_preference.name)
    end
  end

  scenario 'and a new recipe with new food preference' do
    admin = create(:admin)
    login_as_admin(admin)

    food_preference = build(:food_preference)
    recipe = build(:recipe, food_preference: food_preference, user:admin)
    visit new_food_preference_path

    fill_in 'food_preference[name]', with: food_preference.name
    
    click_on 'Criar nova preferência'

    visit new_recipe_path

    create_new_recipe_by_browser(recipe)

    click_on 'Criar receita'

    recipe = Recipe.last

    expect(current_path).to eq(recipe_path(recipe))
    check_recipe_is_present(recipe)
  end

  scenario 'with blank fields' do
    admin = create(:admin)
    login_as_admin(admin)

    visit new_food_preference_path
    click_on 'Criar nova preferência'
    expect(page).to have_content("Must give the food preference\'s name!")
  end

  scenario 'with too long name' do
    admin = create(:admin)
    login_as_admin(admin)

    food_preference = build(:food_preference_with_long_name)
    visit new_food_preference_path

    fill_in 'food_preference[name]', with: food_preference.name
    click_on 'Criar nova preferência'

    expect(page).to have_content("Name is too long, must have at least 30 characters")
  end

  scenario 'with already existent food preference name' do
    admin = create(:admin)
    login_as_admin(admin)

    food_preference = build(:food_preference)
    visit new_food_preference_path

    fill_in 'food_preference[name]', with: food_preference.name
    click_on 'Criar nova preferência'

    food_preference = FoodPreference.last

    expect(current_path).to eq(food_preference_path(food_preference))

    visit new_food_preference_path

    fill_in 'food_preference[name]', with: food_preference.name
    click_on 'Criar nova preferência'

    expect(page).to have_content("This preference already exists!")

    # This next line doesn't work, because propbably respond_with uses method
    # render instead of redirect_to. Besides, the error message appear to the
    # user
    #
    # expect(current_path).to eq(new_food_preference_path)
  end
end
