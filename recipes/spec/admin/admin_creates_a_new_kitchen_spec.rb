require 'rails_helper'

feature 'admin creates a new kitchen' do
  scenario 'successfully' do
    admin = create(:admin)
    login_as_admin(admin)

    kitchen = build(:kitchen)
    visit new_kitchen_path

    fill_in 'kitchen[name]', with: kitchen.name
    click_on 'Criar nova cozinha'

    expect(page).to have_content(kitchen.name)

    # checks the aside menu, where are all registered kitchens
    within('aside') do
      expect(page).to have_content(kitchen.name)
    end
  end

  scenario 'and a new recipe with new kitchen' do
    admin = create(:admin)
    login_as_admin(admin)

    kitchen = build(:kitchen)
    recipe = build(:recipe, kitchen: kitchen, user:admin)
    visit new_kitchen_path

    fill_in 'kitchen[name]', with: kitchen.name
    click_on 'Criar nova cozinha'

    visit new_recipe_path

    create_new_recipe_by_browser(recipe)

    click_on 'Criar receita'

    recipe = Recipe.last

    expect(current_path).to eq(recipe_path(recipe))
    check_recipe_is_present(recipe)
  end

  scenario 'with blank fields' do
    admin = create(:admin)
    login_as_admin(admin)

    visit new_kitchen_path
    click_on 'Criar nova cozinha'
    expect(page).to have_content("Must give the kitchen\'s name!")
  end

  scenario 'with too long name' do
    admin = create(:admin)
    login_as_admin(admin)

    kitchen = build(:kitchen_with_long_name)
    visit new_kitchen_path

    fill_in 'kitchen[name]', with: kitchen.name
    click_on 'Criar nova cozinha'

    expect(page).to have_content("Name is too long, must have at least 30 characters")
  end

  scenario 'with already existent kitchen name' do
    admin = create(:admin)
    login_as_admin(admin)

    kitchen = build(:kitchen)
    visit new_kitchen_path

    fill_in 'kitchen[name]', with: kitchen.name
    click_on 'Criar nova cozinha'

    kitchen = Kitchen.last

    expect(current_path).to eq(kitchen_path(kitchen))

    visit new_kitchen_path

    fill_in 'kitchen[name]', with: kitchen.name
    click_on 'Criar nova cozinha'

    expect(page).to have_content("This kitchen already exists!")

    # This next line doesn't work, because propbably respond_with uses method
    # render instead of redirect_to. Besides, the error message appear to the
    # user
    #
    # expect(current_path).to eq(new_kitchen_path)
  end
end
