require 'rails_helper'

feature 'admin creates a new food type' do
  scenario 'successfully' do
    admin = create(:admin)
    login_as_admin(admin)

    food_type = build(:food_type)
    visit new_food_type_path

    fill_in 'food_type[name]', with: food_type.name
    click_on 'Criar novo tipo de comida'

    expect(page).to have_content(food_type.name)

    # checks the aside menu, where are all registered food_types
    within('aside') do
      expect(page).to have_content(food_type.name)
    end
  end

  scenario 'and a new recipe with new food_type' do
    admin = create(:admin)
    login_as_admin(admin)

    food_type = build(:food_type)
    recipe = build(:recipe, food_type: food_type, user:admin)
    visit new_food_type_path

    fill_in 'food_type[name]', with: food_type.name
    click_on 'Criar novo tipo de comida'

    visit new_recipe_path

    create_new_recipe_by_browser(recipe)

    click_on 'Criar receita'

    recipe = Recipe.last

    expect(current_path).to eq(recipe_path(recipe))
    check_recipe_is_present(recipe)
  end

  scenario 'with blank fields' do
    admin = create(:admin)
    login_as_admin(admin)

    visit new_food_type_path
    click_on 'Criar novo tipo de comida'
    expect(page).to have_content("Must give the food_type\'s name!")
  end

  scenario 'with too long name' do
    admin = create(:admin)
    login_as_admin(admin)

    food_type = build(:food_type_with_long_name)
    visit new_food_type_path

    fill_in 'food_type[name]', with: food_type.name
    click_on 'Criar novo tipo de comida'

    expect(page).to have_content("Name is too long, must have at least 30 characters")
  end

  scenario 'with already existent food_type name' do
    admin = create(:admin)
    login_as_admin(admin)

    food_type = build(:food_type)
    visit new_food_type_path

    fill_in 'food_type[name]', with: food_type.name
    click_on 'Criar novo tipo de comida'

    food_type = FoodType.last

    expect(current_path).to eq(food_type_path(food_type))

    visit new_food_type_path

    fill_in 'food_type[name]', with: food_type.name
    click_on 'Criar novo tipo de comida'

    expect(page).to have_content("This food type already exists!")

    # This next line doesn't work, because propbably respond_with uses method
    # render instead of redirect_to. Besides, the error message appear to the
    # user
    #
    # expect(current_path).to eq(new_kitchen_path)
  end
end
