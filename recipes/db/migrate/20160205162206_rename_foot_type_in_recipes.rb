class RenameFootTypeInRecipes < ActiveRecord::Migration
  def change
    rename_column :recipes, :foot_type, :food_type
  end
end
