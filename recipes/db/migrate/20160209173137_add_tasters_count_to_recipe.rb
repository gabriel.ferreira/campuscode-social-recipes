class AddTastersCountToRecipe < ActiveRecord::Migration
  def change
    add_column :recipes, :tasters_count, :integer
  end
end
