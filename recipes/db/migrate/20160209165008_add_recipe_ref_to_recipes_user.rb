class AddRecipeRefToRecipesUser < ActiveRecord::Migration
  def change
    add_reference :recipes_users, :recipe, index: true
    add_foreign_key :recipes_users, :recipes
  end
end
