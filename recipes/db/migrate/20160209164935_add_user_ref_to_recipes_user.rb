class AddUserRefToRecipesUser < ActiveRecord::Migration
  def change
    add_reference :recipes_users, :user, index: true
    add_foreign_key :recipes_users, :users
  end
end
