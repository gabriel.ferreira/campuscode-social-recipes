class AddKitchenRefToRecipes < ActiveRecord::Migration
  def change
    add_reference :recipes, :kitchen, index: true
    add_foreign_key :recipes, :kitchens
  end
end
