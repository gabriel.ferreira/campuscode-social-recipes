class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.string :kitchen
      t.string :foot_type
      t.string :food_preference
      t.integer :total_people
      t.integer :total_time
      t.string :difficulty
      t.text :ingredients
      t.text :preparation

      t.timestamps null: false
    end
  end
end
