$(document).ready(function(){
  $('.aside-menu h4 a').click(function(){
    link = $(this);

    span_tag = link.find('span')

    if(span_tag.hasClass('glyphicon glyphicon-menu-down')){
      span_tag.addClass('glyphicon-menu-up')
      span_tag.removeClass('glyphicon-menu-down')
    }else{
      span_tag.addClass('glyphicon-menu-down')
      span_tag.removeClass('glyphicon-menu-up')
    }
  })
})
