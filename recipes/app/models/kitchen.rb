class Kitchen < ActiveRecord::Base
  validates :name, presence: {message: "Must give the kitchen\'s name!"}
  validates :name, uniqueness: {message: "This kitchen already exists!"}
  validates :name, length: {
    maximum: 30,
    too_long: "Name is too long, must have at least %{count} characters"
  }
  has_many :recipes
end
