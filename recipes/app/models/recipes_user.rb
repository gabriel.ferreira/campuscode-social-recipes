class RecipesUser < ActiveRecord::Base
  belongs_to :user, counter_cache: :favorites_count
  belongs_to :recipe, counter_cache: :tasters_count
end
