class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, uniqueness: {message: 'Ops! This name has already been taken.'}
  validates :name, presence: {message: 'Please, present yourself!'}
  validates :location, presence: {message: 'Please, tell us at least your city!'}
  has_many :recipes

  has_many :recipes_users, dependent: :destroy
  has_many :favorites, through: :recipes_users, source: :recipe
end
