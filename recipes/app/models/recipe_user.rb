class RecipesUser < ActiveRecord::Base
  belongs_to :user, counter_cache: :recipes_count
  belongs_to :recipe, counter_cache: :users_count
end
