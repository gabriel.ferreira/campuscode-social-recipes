class FoodPreference < ActiveRecord::Base
  validates :name, presence:{ message: "Must give the food preference\'s name!" }
  validates :name, uniqueness: {message: "This preference already exists!"}
  validates :name, length: {
    maximum: 30,
    too_long: "Name is too long, must have at least %{count} characters"
  }
  has_many :recipes
end
