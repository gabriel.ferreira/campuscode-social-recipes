class Recipe < ActiveRecord::Base
  validates :name, length: { maximum: 30, too_long: 'The name is too long.' }
  validates :name, presence: {message: 'Must give a recipe name.'}
  validates :ingredients, presence: {message: "Must fill in the recipe\'s ingredients"}
  validates :preparation, presence: {message: "Must fill in the recipe\'s preparation"}
  validates_associated :kitchen, presence: {message: 'Must give a recipe kitchen.'}
  validates_associated :food_type, presence: {message:"Must specify the recipe\'s food type"}
  validates_associated :food_preference, presence: {message: "Must specify the recipe\'s food preference"}

  has_many :recipes_users, dependent: :destroy
  has_many :tasters, through: :recipes_users, source: :user

  belongs_to :user
  belongs_to :food_preference
  belongs_to :food_type
  belongs_to :kitchen

  has_attached_file :photo
  validates_attachment_content_type :photo, :content_type => %w(image/jpeg image/jpg image/png)


  scope :last_recipes, ->{ all.order(created_at: :desc).limit(20) }
  scope :most_favorites, ->{ where.not(tasters_count: 0).order(tasters_count: :desc).limit(20) }
  DIFFICULTIES = %w(easy medium hard).freeze

  def self.difficulties
    DIFFICULTIES
  end

end
