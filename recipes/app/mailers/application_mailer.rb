class ApplicationMailer < ActionMailer::Base
  default from: "socialrecipes.gabrielferreira@gmail.com"
  layout 'mailer'
end
