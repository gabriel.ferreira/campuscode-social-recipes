class RecipeMailer < ApplicationMailer
  
  def send_recipe_to_friend(recipe, email)
    @recipe = recipe
    @recipe_url = recipe_url(recipe)
    mail(to: email,
         subject: "Uma receita para você!")
  end

end
