class RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!
  protected

  def after_update_path_for(resource)
    user_path(resource)
  end

  # def sign_up_params
  #   params.require(:user).permit(:name, :location, :email, :password,
  #                                :password_confirmation)
  # end
  #
  # def account_update_params
  #   params.require(:user).permit(:name, :location, :email, :password,
  #                                :password_confirmation, :current_password)
  # end
end
