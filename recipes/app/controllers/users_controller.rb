class UsersController < ApplicationController
  before_action :authenticate_user!, except: :show
  before_action :get_user, except: :show


  def show
    @user = User.find(params[:id]).decorate
    @last_recipes = @user.recipes
    @favorites = @user.favorites
  end

  def create_favorite
    @recipe = Recipe.find params[:recipe_id]
    @user.favorites << @recipe
    redirect_to user_path(@user)
  end

  def delete_favorite
    @recipe = Recipe.find params[:id]
    @user.favorites.delete @recipe
    redirect_to user_path(@user)
  end

  private

  def get_user
    @user = User.find( current_user.id )
  end
end
