require "application_responder"

class ApplicationController < ActionController::Base
  before_action :set_collections
  before_filter :configure_permitted_parameters, if: :devise_controller?

  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def set_collections
    @food_preferences = FoodPreference.all
    @food_types = FoodType.all
    @kitchens = Kitchen.all
  end

  def only_admin!
    redirect_to new_user_session_path unless is_admin?
  end

  def after_sign_in_path_for(resource)
    user_path(current_user)
  end

  protected

  def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :location, :password, :password_confirmation) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :current_password, :location) }
  end

  private

  def is_admin?
    current_user && current_user.admin?
  end

  def redirect_to_user_path
    redirect_to new_user_session_path
  end
end
