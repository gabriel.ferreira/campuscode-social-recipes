class FoodTypesController < ApplicationController
  before_action :only_admin!, only: [:new, :create]
  def show
    @food_type = FoodType.find(params[:id]).decorate
  end

  def new
    @food_type = FoodType.new
  end

  def create
    @food_type = FoodType.create food_type_params
    respond_with @food_type
  end

  private

  def food_type_params
    params.require(:food_type).permit(:name)
  end
end
