class FoodPreferencesController < ApplicationController
  before_action :only_admin!, only: [:new, :create]
  def new
    @food_preference = FoodPreference.new
  end

  def create
    @food_preference = FoodPreference.create food_preference_params
    respond_with @food_preference
  end

  def show
    @food_preference = FoodPreference.find(params[:id]).decorate
  end

  private

  def food_preference_params
    params.require(:food_preference).permit(:name)
  end
end
