class KitchensController < ApplicationController
  before_action :only_admin!, only: [:new, :create]

  def show
    @kitchen = Kitchen.find(params[:id]).decorate
  end

  def new
    @kitchen = Kitchen.new
  end

  def create
    @kitchen = Kitchen.create kitchen_params
    respond_with @kitchen
  end

  private

  def kitchen_params
    params.require(:kitchen).permit(:name)
  end
end
