class RecipesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :get_recipe, only: [:edit, :update, :destroy]
  def index
    @last_recipes = Recipe.last_recipes.decorate
    @favorites = Recipe.most_favorites.decorate
  end

  def new
    @recipe = Recipe.new
  end

  def create
    @recipe = current_user.recipes.create recipe_params
    respond_with @recipe
  end

  def show
    @recipe = Recipe.find(params[:id]).decorate
  end

  def edit
  end

  def update
    @recipe.update recipe_params
    respond_with @recipe
  end

  def destroy
    @recipe.destroy
    redirect_to user_path(current_user)
  end

  def mail_to_friend
    @recipe = Recipe.find(params[:id])
    @user = User.find_by(email: params[:email])
    RecipeMailer.send_recipe_to_friend( @recipe, @user.email ).deliver
    redirect_to @recipe
  end

  def send_to_friend
    @recipe = Recipe.find(params[:id])
  end

  private

  def get_recipe
    @recipe = Recipe.find params[:id]
  end

  def recipe_params
    params.require(:recipe)
          .permit(:name, :kitchen_id, :food_type_id, :food_preference_id,
                  :total_people, :total_time, :difficulty, :ingredients,
                  :preparation, :photo)
  end
end
