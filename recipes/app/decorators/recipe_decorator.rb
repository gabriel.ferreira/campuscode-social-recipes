class RecipeDecorator < Draper::Decorator
  delegate_all
  include Draper::LazyHelpers

  def as_favorite
    return nil unless user_signed_in?
    return unset_favorite if is_favorite?
    set_favorite
  end

  def editable
    return nil unless is_editable?
    result = h.link_to 'Editar', edit_recipe_path(recipe), class: 'btn btn-default'
    result += h.link_to 'Deletar', recipe_path(recipe), method: :delete,
                           data:{ confirm: 'Tem certeza?' },
                           class: 'btn btn-default'
  end

  def email_to
    return nil unless user_signed_in?
    link_to 'Enviar para um amigo', send_to_friend_recipe_path(current_user, @recipe),
          class: 'btn btn-default'
  end

  protected

  def set_favorite
    link_to 'Marcar favorita',
               create_favorite_user_path( current_user, recipe_id: recipe.id ),
               method: :post, class: 'btn btn-warning'
  end

  def unset_favorite
    link_to 'Desmarcar',
               delete_favorite_user_path( recipe.id ),
               method: :delete, class: 'btn btn-default'
  end

  def is_editable?
    current_user == recipe.user and request.original_fullpath != root_path
  end

  def is_favorite?
    object.tasters.include? current_user
  end

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end
