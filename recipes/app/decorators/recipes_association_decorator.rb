class RecipesAssociationDecorator < Draper::Decorator
  delegate_all

  def recipes
    object.recipes.decorate
  end

  def list_recipes
    return h.render recipes if recipes.any?
    "Não há receitas para #{object.name}"
  end
end
